import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    loading: true,
    person: "",
  }

  async componentDidMount() {
    const url = "https://api.randomuser.me/";
    const response = await fetch(url);
    const data = await response.json();
    this.setState({
      person: data.results[0],
      loading: false,
    });
    console.log(data.results[0]);
  }
  render() {
    return (
      <>
        {this.state.loading || !this.state.person ? (
          <div>loading...</div>
        ) : (
            <div>
              <div>{this.state.person.name.title}</div>
              <div>{this.state.person.name.first}</div>
              <div> {this.state.person.name.last}</div>
              <img
                src={this.state.person.picture.large}
                alt={this.state.person.picture.large}
              />
            </div>
          )}
      </>
    );
  }
}



export default App;
